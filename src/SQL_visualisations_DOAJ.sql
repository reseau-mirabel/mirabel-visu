-- Requêtes SQL visualisations DOAJ

-- Tous les titres du nouveau corpus
-- File: doaj_france_corpus_m
SELECT DISTINCT t.*, issnpapier.issn AS 'issnpapier', issnelec.issn AS 'issnelec'
FROM Titre t
    LEFT OUTER JOIN Issn issnpapier ON t.id = issnpapier.titreId AND issnpapier.support = 'papier'
    LEFT OUTER JOIN Issn issnelec ON t.id = issnelec.titreId AND issnelec.support = 'electronique'
    JOIN Titre_Editeur te ON t.id = te.titreId
    JOIN Editeur e ON e.id = te.editeurId
    JOIN Issn I ON t.id = I.titreId
    LEFT OUTER JOIN AttributTitre at ON at.titreId = t.id
    LEFT OUTER JOIN Sourceattribut s ON s.id = at.sourceattributId
    JOIN LienTitre lt ON t.id = lt.titreId
WHERE EXISTS (
    SELECT 1
    FROM LienTitre lt2
    WHERE lt2.sourceId = 1
        AND lt2.titreId = t.id)
    AND (
        EXISTS (
            SELECT 1
            FROM AttributTitre at3
            JOIN Sourceattribut s3 ON s3.id = at3.sourceattributId
            WHERE at3.titreId = t.id
                AND s3.identifiant = 'doaj-france')
        OR
        (e.paysId = 62 AND ancien = false))
;


-- Liste des titres français dans le DOAJ avec ISSNs
-- File: doaj_france_m
WITH R1 AS (
    SELECT Titre.*, Issn.issn AS 'Papier'
    FROM Issn, Titre
    WHERE Titre.id = Issn.titreId
        AND Issn.support = 'papier'
        AND Titre.id IN (
            SELECT Titre_Editeur.titreId
            FROM Titre_Editeur JOIN Editeur
            WHERE Titre_Editeur.editeurId = Editeur.id
                AND paysId = 62 -- France
                AND ancien = 0
                AND Titre_Editeur.titreId IN (
                    SELECT LienTitre.titreId
                    FROM LienTitre
                    WHERE LienTitre.name = 'DOAJ'
                )
        )
),
R2 AS (
    SELECT Titre.*, Issn.issn AS 'Electronique'
    FROM Issn, Titre
    WHERE Titre.id = Issn.titreId
        AND Issn.support = 'electronique'
        AND Titre.id IN (
            SELECT Titre_Editeur.titreId
            FROM Titre_Editeur JOIN Editeur
            WHERE Titre_Editeur.editeurId = Editeur.id
                AND paysId = 62 -- France
                AND ancien = 0
                AND Titre_Editeur.titreId IN (
                    SELECT LienTitre.titreId
                    FROM LienTitre
                    WHERE LienTitre.name = 'DOAJ'
                )
        )
)
SELECT R2.*, R1.Papier
FROM R1 RIGHT JOIN R2 ON R1.id = R2.id
;

-- Liste des accès des titres français dans le DOAJ (sans éditeur précédent)
-- File: doaj_plateformes
SELECT Service.*, Ressource.nom, Titre.titre, issn_p.issn AS 'Papier', issn_e.issn AS 'Electronique'
FROM Service
    JOIN Ressource ON Service.ressourceId = Ressource.id
    JOIN Titre ON Service.titreId = Titre.id
    LEFT OUTER JOIN Issn AS issn_e ON Titre.id = issn_e.titreId and issn_e.support = 'electronique'
     LEFT OUTER JOIN Issn AS issn_p on Titre.id = issn_p.titreId AND issn_p.support = 'papier'
WHERE Service.type = 'Intégral'
    AND Service.titreId IN (
        SELECT Titre.id
        FROM Titre
        WHERE id IN (
            SELECT Titre_Editeur.titreId
            FROM Titre_Editeur
                JOIN Editeur ON Titre_Editeur.editeurId = Editeur.id
            WHERE paysId = 62
                AND ancien = 0
                AND titreId IN (SELECT titreId FROM LienTitre WHERE name = 'DOAJ')
        )
    )
;

-- Liste des éditeurs des titres français dans le DOAJ
-- File: doaj_editeurs
SELECT Titre_Editeur.*, Editeur.nom, Editeur.role AS 'roleEditeur', issn_p.issn as 'Papier', issn_e.issn AS 'Electronique'
FROM Titre_Editeur
    JOIN Editeur ON Titre_Editeur.editeurId = Editeur.id
    LEFT OUTER JOIN Titre ON Titre_Editeur.titreId = Titre.id
    LEFT OUTER JOIN Issn AS issn_e ON Titre.id = issn_e.titreId and issn_e.support = 'electronique'
    LEFT OUTER JOIN Issn AS issn_p on Titre.id = issn_p.titreId AND issn_p.support = 'papier'
WHERE Titre_Editeur.titreId IN (SELECT titreId FROM LienTitre WHERE name = 'DOAJ')
    AND paysId = 62
    AND ancien = 0
;

-- Historiques des titres français dans le DOAJ
-- File: doaj_historique
SELECT *
FROM Titre
WHERE revueId IN (
    SELECT DISTINCT Titre.revueId
    FROM Titre
    WHERE id IN (
        SELECT Titre_Editeur.titreId
        FROM Titre_Editeur
            JOIN Editeur ON Titre_Editeur.editeurId = Editeur.id
        WHERE paysId = 62
            AND ancien = 0
            AND titreId IN (SELECT titreId FROM LienTitre WHERE name = 'DOAJ')
    )
)
;
