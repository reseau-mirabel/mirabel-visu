import csv
import re
import sys

import mysql.connector
import pandas as pd
import requests
import urllib3


def create_csv_from_sql_request(path, conn, filename, sql):
    cursor = conn.cursor()
    cursor.execute(sql)
    csv_path = f"{path}/{filename}.csv"
    with open(csv_path, 'w', newline='') as csvfile:
        writer = csv.writer(csvfile)
        writer.writerow([column[0] for column in cursor.description])
        writer.writerows(cursor.fetchall())


def parse_db_config(config_path):
    with open(config_path, 'r') as f:
        config_content = f.read()

    connection_string = re.search(r"mysql:host=([^;]+);dbname=([^']+)", config_content)
    host = connection_string.group(1)
    database = connection_string.group(2)
    username = re.search(r"'username'\s*=>\s*'([^']+)'", config_content).group(1)
    password = re.search(r"'password'\s*=>\s*'([^']*)'", config_content).group(1)

    return {
        'host': host,
        'database': database,
        'user': username,
        'password': password
    }


def get_titre_fr_in_m(conn):
    cursor = conn.cursor()
    query = """
        SELECT DISTINCT I.issn
        FROM Titre t
        JOIN Titre_Editeur te ON t.id = te.titreId
        JOIN Editeur e ON e.id = te.editeurId
        JOIN Issn I ON t.id = I.titreId
        JOIN AttributTitre at ON at.titreId = t.id
        JOIN Sourceattribut s ON s.id = at.sourceattributId
        JOIN LienTitre lt ON t.id = lt.titreId
        WHERE e.paysId = 62
          AND EXISTS (
            SELECT 1
            FROM LienTitre lt2
            WHERE lt2.sourceId= 1 AND lt2.titreId=t.id
          )
          AND NOT EXISTS (
            SELECT 1
            FROM AttributTitre at3
            JOIN Sourceattribut s3 ON s3.id = at3.sourceattributId
            WHERE at3.titreId = t.id AND s3.identifiant = 'doaj-france'
          );
        """
    cursor.execute(query)
    result = cursor.fetchall()
    issn_list = [row[0] for row in result]
    return issn_list


def fetch_revues_en_plus_to_file(path, conn):
    issn_titres_fr = get_titre_fr_in_m(conn)
    df_journal = pd.read_csv(f"{path}/journalcsv.csv")
    df_journal = df_journal[df_journal["Journal ISSN (print version)"].isin(issn_titres_fr) &
                            (~df_journal["Country of publisher"].eq("France"))
                            ]
    df_journal.to_csv(path + "/revues_en_plus.csv", sep=",", index=False)
    return df_journal


if __name__ == "__main__":
    path = "./Graphiques_DOAJ/donnee"
    if len(sys.argv) == 2:
        db_conf_path = sys.argv[1]
    else:
        print('Yii local config file needed')
        exit()
    print("Creating CSV...")
    # Download journal.csv
    try:
        response = requests.get("https://doaj.org/csv")
        with open(path + "/journalcsv.csv", "wb") as f:
            f.write(response.content)
    except urllib3.exceptions.NameResolutionError as e:
        print("Erreur DNS : %s" % e)
    except Exception as e:
        raise e
    # Create db connexion
    db_conf = parse_db_config(db_conf_path)
    try:
        conn = mysql.connector.connect(
            host=db_conf["host"],
            user=db_conf["user"],
            password=db_conf["password"],
            database=db_conf["database"]
        )
        # Creating data csv from sql
        start = re.compile(r'^-- File: (\S+)\s*$')
        end = re.compile(r'^;')
        filename = ''
        with open("./src/SQL_visualisations_DOAJ.sql", 'r', encoding="utf-8") as f:
            for line in f:
                r = start.match(line)
                if (r):
                    filename = r.group(1)
                    sql = ''
                elif (end.match(line)):
                    create_csv_from_sql_request(path, conn, filename, sql)
                    sql = ''
                elif filename != '':
                    sql = sql + line
        # Creating revue_en_plus.csv
        fetch_revues_en_plus_to_file("Graphiques_DOAJ/donnee", conn)
        print("Csv created")
    except Exception as e:
        raise e

    finally:
        conn.close()
