import ast
import json
import math
import re
import sys
import textwrap
from collections import Counter

# Librairies
import networkx as nx
import numpy as np
import pandas as pd
import plotly.express as px
import plotly.graph_objects as go
import plotly.io as pio
import requests
from dateutil.relativedelta import relativedelta
from plotly.subplots import make_subplots

# Constantes
FOLDER = "./Graphiques_DOAJ"
OUTPUT_FOLDER = FOLDER + "/resultat"
OUTPUT_FORMAT = "html"
ADDED_ON_DOAJ_BEFORE = None

def write_graph(graph, output_file, output_format):
    output_path = f"{OUTPUT_FOLDER}/{output_file}.{output_format}"
    if output_format == "html":
        graph.write_html(
            output_path,
            full_html=False,
            include_plotlyjs=False,
        )
    elif output_format == "png":
        graph.write_image(output_path)

# %% Liste des ajouts depuis début M22


def revues_ajoutees_M22(f_doaj_france_complet):
    f_doaj_france_complet["Added on Date"] = pd.to_datetime(
        f_doaj_france_complet["Added on Date"]
    )
    f_doaj_france_complet = f_doaj_france_complet.loc[
        f_doaj_france_complet["Added on Date"] > "2022-03-01"
    ]

    f_doaj_france_complet.to_csv(
        "{}/resultat/M22_revues_doaj.csv".format(str(FOLDER)),
        sep="\t",
        encoding="utf-8",
        index=False,
    )


# %% 1. Courbe ajouts des revues françaises dans DOAJ

def graphique_courbes(f_doaj_france_complet, f_doaj_france):
    def data_courbes(df_doaj):
        df_doaj["date_ajout"] = pd.to_datetime(df_doaj["Added on Date"])
        df = df_doaj["date_ajout"].value_counts().sort_index().cumsum().reset_index()
        df.columns = ["date ajout", "cumsum"]
        return df

    data_doaj_france_complet = data_courbes(f_doaj_france_complet)
    data_doaj_france = data_courbes(f_doaj_france)

    fig = go.Figure()
    fig.add_trace(go.Scatter(x=data_doaj_france_complet["date ajout"], y=data_doaj_france_complet["cumsum"],
                             mode='lines', name="Revues FRANCE selon Mir@bel"))
    fig.add_trace(go.Scatter(x=data_doaj_france["date ajout"], y=data_doaj_france["cumsum"],
                             mode='lines', name="Revues FRANCE dans DOAJ"))

    fig.update_layout(
        title="""Nombre de revues "françaises" dans le DOAJ depuis son lancement (2003)""",
        xaxis_title="Années",
        yaxis_title="Nombre de revues"
    )

    fig.add_annotation(x=data_doaj_france["date ajout"].iloc[-1],
                       y=data_doaj_france["cumsum"].iloc[-1],
                       text=str(len(f_doaj_france)),
                       showarrow=True,
                       arrowhead=1)

    fig.add_annotation(x=data_doaj_france_complet["date ajout"].iloc[-1],
                       y=data_doaj_france_complet["cumsum"].iloc[-1],
                       text=str(len(f_doaj_france_complet)),
                       showarrow=True,
                       arrowhead=1)
    write_graph(fig, "1_courbes", OUTPUT_FORMAT)




def graphique_variation_annuelle(f_doaj_france_complet):
    def data_courbes(df_doaj):
        df_doaj["date_ajout"] = pd.to_datetime(df_doaj["Added on Date"])
        df = df_doaj["date_ajout"].value_counts().sort_index().reset_index()
        df.columns = ["date ajout", "nombre"]
        return df

    data_doaj_france_complet = data_courbes(f_doaj_france_complet)

    # Variation annuelle moyenne
    variation_annuelle = data_doaj_france_complet.groupby(
        data_doaj_france_complet["date ajout"].dt.year
    )["nombre"].sum().reset_index()
    variation_annuelle["cumsum"] = variation_annuelle["nombre"].cumsum()
    variation_annuelle["date ajout"] = pd.to_datetime(variation_annuelle["date ajout"], format="%Y")

    fig = go.Figure()

    fig.add_trace(go.Scatter(
        x=variation_annuelle["date ajout"],
        y=variation_annuelle["nombre"],
        mode='lines+markers',
        line=dict(color='red', width=2),
        marker=dict(size=8),
    ))

    # Ajout des annotations pour chaque point
    for i, row in variation_annuelle.iterrows():
        fig.add_annotation(
            x=row["date ajout"] + relativedelta(months=2),
            y=row["nombre"],
            text=str(row["nombre"]),
            showarrow=False,
            yshift=10,
            font=dict(size=10)
        )

    fig.update_layout(
        title="Variation annuelle des ajouts des revues françaises dans le DOAJ",
        xaxis_title="Années",
        yaxis_title="Nombre de revues",
        height=600,
        width=1000,
        xaxis=dict(
            dtick="M24",
            tickformat="%Y"
        )
    )

    write_graph(fig, "10_variation_ajouts_doaj", OUTPUT_FORMAT)


# %% 2. Répartition du sceau DOAJ

def sceau_doaj(f_doaj_france_complet):
    data = f_doaj_france_complet["DOAJ Seal"].value_counts()
    data = data.rename({"No": "Non", "Yes": "Oui"})

    fig = go.Figure(data=[go.Pie(labels=data.index, values=data.values)])
    fig.update_traces(textposition='inside', textinfo='percent+label')
    fig.update_layout(title="""Part des revues "françaises" ayant obtenu le sceau DOAJ""")
    write_graph(fig, "2_repartition_doaj_seal", OUTPUT_FORMAT)


# %% 3. Répartition APC sans APC

def graphiques_apc(f_doaj_france_complet):
    data = f_doaj_france_complet["APC"].value_counts()
    # Nettoyage colonne devise
    f_doaj_france_complet["APC amount"] = f_doaj_france_complet["APC amount"].fillna("")
    f_doaj_france_complet["APC amount"] = [
        (
            "{0} EUR".format(re.search(r"\d+(?=\sEUR)", s).group())
            if re.search(r"\d+(?=\sEUR)", s) is not None
            else s
        )
        for s in list(f_doaj_france_complet["APC amount"])
    ]
    f_doaj_france_complet[["APC amount", "Devise"]] = f_doaj_france_complet["APC amount"].str.split(" ", n=1,
                                                                                                    expand=True)
    f_doaj_france_complet["APC amount"] = f_doaj_france_complet["APC amount"].replace("", 0)
    f_doaj_france_complet["APC amount"] = f_doaj_france_complet["APC amount"].astype(
        int
    )
    # Conversion en euros
    f_doaj_france_complet.loc[
        f_doaj_france_complet["Devise"] == "USD", "APC amount"
    ] = (f_doaj_france_complet["APC amount"] * 0.95).astype(int)
    f_doaj_france_complet.loc[
        f_doaj_france_complet["Devise"] == "CNY", "APC amount"
    ] = (f_doaj_france_complet["APC amount"] * 0.15).astype(int)
    f_doaj_france_complet.loc[
        f_doaj_france_complet["Devise"] == "GBP", "APC amount"
    ] = (f_doaj_france_complet["APC amount"] * 1.15).astype(int)

    bins = [0, 499, 1499, 2499, np.inf]
    names = ["<500", "500-1499", "1500-2499", "2500+"]
    f_doaj_france_complet["APC amount range"] = pd.cut(
        f_doaj_france_complet["APC amount"], bins, labels=names
    )

    fig = make_subplots(rows=1, cols=2, specs=[[{'type': 'domain'}, {'type': 'bar'}]])
    fig.add_trace(go.Pie(
        labels=["Non", "Oui"],
        values=[data["No"], data["Yes"]],
        textinfo='percent+value',
        hole=0.3,
        pull=[0, 0.2]
    ), 1, 1)

    data2 = f_doaj_france_complet["APC amount range"].value_counts().sort_index()
    apc_ratios = (data2 / data2.sum() * 100).round(0).astype(int)
    names = ["<500", "500-1499", "1500-2499", "2500+"]
    colors = ['#FF7F0E', '#FFA15A', '#FFCCA5', '#FFE8D6']

    fig = make_subplots(rows=1, cols=2, specs=[[{'type': 'domain'}, {'type': 'xy'}]],
                        column_widths=[0.5, 0.5], horizontal_spacing=0.05)

    # Graphique circulaire
    fig.add_trace(go.Pie(
        labels=["Non", "Oui"],
        values=[data["No"], data["Yes"]],
        textinfo='percent+value',
        textposition='inside',
        pull=[0, 0.2],
        marker_colors=['#1F77B4', '#FF7F0E'],
        textfont_size=14,
        rotation=90
    ), 1, 1)

    # Graphique en barres
    fig.add_trace(go.Bar(
        y=names,
        x=apc_ratios,
        orientation='h',
        marker_color=colors,
        text=apc_ratios.astype(str) + '%',
        textposition='inside',
        insidetextanchor='middle',
        textfont_color='black',
        textfont_size=14,
        name="Montant APC (en €)"
    ), 1, 2)

    # Mise en page
    fig.update_layout(
        title="""Revues "françaises" dans le DOAJ avec frais de publications (APC)""",
        showlegend=False,
        height=500,
        width=1000,
        annotations=[
            dict(text="Montant APC (en €)", x=1.0, y=1.05, showarrow=False, xref="paper", yref="paper", font_size=16)
        ]
    )

    fig.update_yaxes(title_text="", showticklabels=True, col=2)
    fig.update_xaxes(title_text="", showticklabels=False, range=[0, 100], col=2)

    write_graph(fig, "3_repartition_APC", OUTPUT_FORMAT)


# %% 4. Nombre de titres dans le DOAJ par plateforme

def graphique_plateformes(f_doaj_plateformes):
    pio.renderers.default = "browser"
    min_revue = 3

    f_doaj_plateformes = f_doaj_plateformes[f_doaj_plateformes["acces"] == "Libre"]
    f_doaj_plateformes = f_doaj_plateformes[
        f_doaj_plateformes["dateBarrFin"].isin(["2022", "2023", np.nan])
    ]

    duplicates = f_doaj_plateformes[["titre", "nom"]]
    duplicates = duplicates.groupby(duplicates.columns.tolist(), as_index=False).size()
    data = duplicates.iloc[:, :-1]

    data.loc[data["nom"] == "Prairial : plateforme de revues de SHS en accès ouvert", "nom"] = "Prairial"
    data.loc[data[
                 "nom"] == "Preo : pépinière de revues des universités de Bourgogne (uB) et de Franche-Comté (UFC)", "nom"] = "Preo"
    data.loc[data["nom"] == "Plateforme de revues scientifiques de l'Université de Bordeaux", "nom"] = "Open-U Journals"
    data.loc[data["nom"] == "POLEN Pépinière de l'Université Clermont Auvergne", "nom"] = "POLEN"

    data2 = data["nom"].value_counts()
    data2.name = "nb_titres"
    data2 = dict(data2)

    data2.pop("Comptes Rendus de l'Académie des sciences", None)
    data2.pop("European Physical Journal", None)
    data2.pop("HAL", None)

    data2 = {k: v for k, v in data2.items() if v >= min_revue}
    nb_revues = {k: v for k, v in data2.items()}
    data2 = {k: v / 3 for k, v in data2.items()}

    data3 = {}
    liste_titres = list(set(data["titre"]))

    for t in liste_titres:
        df = data[data["titre"] == t].nom.values.tolist()
        data3[t] = df

    data4 = {}
    for f in list(data2.keys()):
        tmp_dict = {}
        for j in list(data2.keys()):
            if j == f:
                next
            else:
                nb = 0
                for test in list(data3.values()):
                    if f in test and j in test:
                        nb += 1
                tmp_dict[j] = nb if nb == 0 else math.log(nb) ** 1.6
        data4[f] = tmp_dict

    G = nx.Graph()
    for plat, size in data2.items():
        G.add_node(plat, size=size, nbrevues=nb_revues[plat])

    pos = nx.spring_layout(G, k=0.125, iterations=50, weight='size')

    center = np.mean(list(pos.values()), axis=0)
    for node in G.nodes():
        weight = G.nodes[node]['size'] / max(data2.values())
        pos[node] = weight * center + (1 - weight) * pos[node]

    node_trace = go.Scatter(
        x=[pos[node][0] for node in G.nodes()],
        y=[pos[node][1] for node in G.nodes()],
        mode='markers+text',
        hoverinfo='text',
        marker=dict(
            color='rgba(135, 206, 250, 0.8)',
            size=[np.power(G.nodes[node]['nbrevues'], 0.6) * 15 for node in G.nodes()],
            line_width=2
        ),
        text=[f"{node}<br>({G.nodes[node]['nbrevues']} revues)" for node in G.nodes()],
        textposition="top center",
        textfont=dict(size=13, color='navy', family="Arial, sans-serif")
    )

    fig = go.Figure(data=[node_trace],
                    layout=go.Layout(
                        title="""Plateformes de diffusion les plus représentées pour les revues "françaises" du DOAJ""",
                        showlegend=False,
                        hovermode='closest',
                        margin=dict(b=20, l=5, r=5, t=40),
                        xaxis=dict(showgrid=False, zeroline=False, showticklabels=False),
                        yaxis=dict(showgrid=False, zeroline=False, showticklabels=False),
                        plot_bgcolor='rgba(240, 240, 240, 0.5)',
                        width=1300,
                        height=900
                    ))

    padding = 0.4
    x_range = [min([pos[node][0] for node in G.nodes()]) - padding,
               max([pos[node][0] for node in G.nodes()]) + padding]
    y_range = [min([pos[node][1] for node in G.nodes()]) - padding,
               max([pos[node][1] for node in G.nodes()]) + padding]
    fig.update_xaxes(range=x_range)
    fig.update_yaxes(range=y_range)
    write_graph(fig, "4_plateformes", OUTPUT_FORMAT)

# %% 5. Nombre de titres dans le DOAJ par éditeur

def graphique_editeurs(f_doaj_editeurs):
    editeurs_count = 22
    data = f_doaj_editeurs["nom"].value_counts()
    counts = list(data.values)
    editeurs = data.index.tolist()
    editeurs[1] = "GERFLINT"
    editeurs[6] = "Académie des Sciences"
    editeurs[10] = "INRAE"
    editeurs[17] = "CCSD"
    # editeurs = [textwrap.fill(string, 40) for string in editeurs]

    editeurs = editeurs[:editeurs_count]
    counts = counts[:editeurs_count]
    bar_colors = [
        "firebrick",
        "sandybrown",
        "skyblue",
        "gold",
        "royalblue",
        "darkslategrey",
        "darkseagreen",
        "lightseagreen",
        "cadetblue",
        "slateblue",
    ]
    # [(np.random.random(), np.random.random(), np.random.random()) for i in editeurs]
    bar_colors = bar_colors * (len(editeurs) // len(bar_colors) + 1)

    fig = go.Figure()
    fig.add_trace(go.Bar(
        y=editeurs,
        x=counts,
        orientation='h',
        marker_color=bar_colors[:len(editeurs)],
        text=counts,
        textposition='outside',
        textfont=dict(size=12),
    ))

    fig.update_layout(
        title="Nombre de titres dans le DOAJ par éditeur",
        xaxis_title="Nb titres",
        yaxis_title="",
        height=800,
        width=1000,
        margin=dict(l=200, r=20, t=40, b=20),  # Ajuster les marges
    )
    write_graph(fig, "5_editeurs", OUTPUT_FORMAT)


# %% Répartition des types d'éditeurs

def types_editeurs(f_doaj_editeurs):
    data = f_doaj_editeurs[["nom", "roleEditeur"]].drop_duplicates(subset=["nom"])
    data = data.reset_index(drop=True)
    data.roleEditeur = data.roleEditeur.fillna("Rôle inconnu")
    data = data["roleEditeur"].value_counts()
    df = pd.DataFrame({"types_editeurs": data.index, "nb": data.values})

    df.to_csv(
        "{}/resultat/types_editeurs.csv".format(str(FOLDER)),
        sep="\t",
        encoding="utf-8",
        index=False,
    )


# %% Répartition de l'âge des revues

def age_revues(f_doaj_historique, f_doaj_france_m, f_doaj_france_complet):
    data = f_doaj_historique.groupby(["revueId"])["dateDebut"].apply(list)
    data = pd.DataFrame({"revueId": data.index, "dateDebut": data.values})
    data["min"] = data.dateDebut.apply(min)

    res_titres = pd.merge(
        f_doaj_france_m, data.drop("dateDebut", axis=1), on="revueId", how="left"
    )
    res_titres[["Electronique", "Papier"]] = res_titres[
        ["Electronique", "Papier"]
    ].fillna("")

    minDate_revue = []
    minDate_titre = []

    # for Issn-p, Issn-e :
    for x, y in zip(f_doaj_france_complet.iloc[:, 5], f_doaj_france_complet.iloc[:, 6]):
        if y in list(res_titres["Electronique"]):
            minDate_revue.append(
                res_titres[res_titres["Electronique"] == y]["min"].values[0]
            )
            minDate_titre.append(
                res_titres[res_titres["Electronique"] == y]["dateDebut"].values[0]
            )
        elif x in list(res_titres["Papier"]):
            minDate_revue.append(res_titres[res_titres["Papier"] == x]["min"].values[0])
            minDate_titre.append(
                res_titres[res_titres["Papier"] == x]["dateDebut"].values[0]
            )
        else:
            minDate_revue.append(0)
            minDate_titre.append(0)

    f_doaj_france_complet["dateDebutRevue"] = minDate_revue
    f_doaj_france_complet["dateDebutTitre"] = minDate_titre

    comp_dates = f_doaj_france_complet.loc[:,
                 ['Journal title', 'dateDebutRevue', 'date_ajout', 'dateDebutTitre', 'Added on Date']]
    # Comp_date ['Journal title', 'dateDebutRevue', 'date_ajout','When did the journal start to publish all content using an open license?', 'Added on Date']
    comp_dates.loc["Added on Date"] = pd.to_datetime(comp_dates["Added on Date"])
    comp_dates.loc["Added on Date"] = comp_dates["Added on Date"].dt.date
    bins = [0, 1900, 1950, 1985, 2000, 2010, 2020, np.inf]
    names = [
        "<=1900",
        "1900-1950",
        "1950-1985",
        "1985-2000",
        "2000-2010",
        "2010-2020",
        "2020+",
    ]
    comp_dates["dateDebutTitre range"] = pd.cut(
        comp_dates["dateDebutTitre"], bins, labels=names
    )

    comp_dates.to_csv(
        "{}/resultat/age_revues.csv".format(str(FOLDER)),
        sep="\t",
        encoding="utf-8",
        index=False,
        float_format="%.0f",
    )

# %% 6. Répartition par langues

def data_langues(data):
    langues_fct = []
    for i in range(len(data)):
        if len(data[i]) == 1:
            langues_fct.append(data[i][0])
        else:
            if all(x in data[i] for x in ["fre", "eng"]) and len(data[i]) == 2:
                langues_fct.append("Français anglais")
            elif all(x not in data[i] for x in ["fre", "eng"]):
                langues_fct.append("Autres langues")
            elif "fre" in data[i] and "eng" not in data[i]:
                langues_fct.append("Français et autre(s) langue(s)")
            elif "eng" in data[i] and "fre" not in data[i]:
                langues_fct.append("Anglais et autre(s) langue(s)")
            else:
                langues_fct.append("Français anglais et autre(s) langue(s)")

    return langues_fct


def graphique_langues(f_doaj_france_m):
    f_doaj_france_m["langues"] = [
        ast.literal_eval(x) for x in f_doaj_france_m["langues"]
    ]
    # ast.literal_eval(x) : transforme un string de la forme "['fre'], ['eng']" en liste

    f_doaj_france_m["langues2"] = data_langues(list(f_doaj_france_m["langues"]))
    data = f_doaj_france_m["langues2"].value_counts()
    data = data.rename("Langues")
    data.rename(
        index={"fre": "Français", "eng": "Anglais", "spa": "Espagnol"}, inplace=True
    )

    # Graphique
    counts = list(data.values)
    langues = data.index.tolist()
    langues = [textwrap.fill(string, 15) for string in langues]
    bar_colors = [
        "dodgerblue",
        "crimson",
        "plum",
        "palevioletred",
        "cadetblue",
        "yellow",
        "lightcoral",
    ]

    fig = go.Figure(data=[go.Bar(x=langues, y=counts, marker_color=bar_colors)])

    for i in range(len(langues)):
        fig.add_annotation(x=langues[i], y=counts[i],
                           text=str(counts[i]),
                           showarrow=False,
                           yshift=10)

    fig.update_layout(
        title="Répartition des langues de publication des revues françaises dans le DOAJ selon Mir@bel",
        xaxis_title="Langues",
        yaxis_title="Nombre de revues",
        width=1400,
        height=900
    )

    write_graph(fig, "6_repartition_langues", OUTPUT_FORMAT)


# %% 7. Répartition par thématiques

def graphique_thematiques(f_doaj_france_m):
    liste_titreid = list(f_doaj_france_m["id"])

    titres_themes = pd.DataFrame()

    for i in liste_titreid:
        url = requests.get("https://reseau-mirabel.info/api/themes/titre/{}".format(i))
        text = url.text
        json_data = json.loads(text)
        titre = pd.json_normalize(json_data)
        titre["idTitre"] = i
        titres_themes = pd.concat([titres_themes, titre], axis=0)

    titres_themes = titres_themes.replace(
        to_replace=r"^Sociologie.+", value="Sociologie", regex=True
    )
    titres_themes = titres_themes[
        ~titres_themes["parentid"].isin(
            [82, 83, 99, 100, 101, 102, 103, 104, 84, 88, 91, 95, 139]
        )
    ]

    # Récupération de tous les thèmes Mir@bel
    url = requests.get("https://reseau-mirabel.info/api/themes")
    text = url.text
    json_data = json.loads(text)
    all_themes = pd.json_normalize(json_data)
    all_themes = all_themes.replace(
        to_replace=r"^Sociologie.+", value="Sociologie", regex=True
    )

    themes = []
    for titreid in liste_titreid:
        theme_temp = titres_themes[titres_themes["idTitre"] == titreid].to_dict("list")
        theme_temp = {"nom": theme_temp["nom"], "parentid": theme_temp["parentid"]}
        themes.append(theme_temp)

    f_doaj_france_m["themes"] = themes
    data = f_doaj_france_m[["id", "titre", "themes"]]

    # Graphique
    themes_rang1 = all_themes.loc[all_themes["parentid"].isin([81, 82, 83])]

    count = []
    for row in data.itertuples():
        temp = []
        temp2 = []
        titre = row.themes
        for j in range(len(titre["nom"])):
            if titre["nom"][j] in list(themes_rang1["nom"]):
                temp.append(titre["nom"][j])
            else:
                parent = themes_rang1.loc[themes_rang1["id"] == titre["parentid"][j]][
                    "nom"
                ].values
                temp.append(titre["nom"][j])
                temp2.append(parent[0])
        if temp2:
            cnt = Counter(temp2)
            for x, y in zip(cnt.keys(), cnt.values()):
                if x in temp:
                    pass
                    # temp += (y-1) * [x]
                    # Ajoute plusieurs fois la même valeur à la liste
                else:
                    temp.append(x)
        count.extend(temp)

    count = Counter(count)
    data2 = [
        [
            x,
            y,
            all_themes[
                all_themes["id"]
                == all_themes[all_themes["nom"] == x]["parentid"].values[0]
            ]["nom"].values[0],
        ]
        for x, y in zip(count.keys(), count.values())
    ]
    data2 = pd.DataFrame(data2)
    data2[2] = [
        re.sub("|".join(["Discipline", "Géographique", "Chronologique"]), "", w)
        for w in list(data2[2])
    ]
    data2 = data2.sort_values(1, ascending=False)

    data3 = dict(thematique=list(data2[0]), parent=list(data2[2]), value=list(data2[1]))

    fig = px.sunburst(data3,
                      title="""Répartition par thématique des revues "françaises" dans le DOAJ""",
                      names="thematique", parents="parent", values="value",
                      width=1200,
                      height=800)
    fig.update_traces(sort=False, selector=dict(type="sunburst"))
    # Désactiver le tri auto
    write_graph(fig, "7_repartition_thematiques", OUTPUT_FORMAT)

# %% 8. Répartition des autres liens


def graphique_autres_liens(f_doaj_france_m):
    res = [json.loads(idx) for idx in f_doaj_france_m["liensJson"]]
    # String Json en liste de dict
    autres_liens = pd.DataFrame([x["src"] for y in res for x in y])
    autres_liens.rename(columns={0: "liens"}, inplace=True)
    data = autres_liens["liens"].value_counts()
    data = data.drop(data.index[0])
    data = data.drop(data.index[11:])

    pio.renderers.default = "browser"
    data2 = dict(data)
    nb_liens = {k: v for k, v in data2.items()}
    data2 = {k: math.log(v) ** 1.8 for k, v in data2.items()}

    # Add node for each plateforme
    midsummer = nx.Graph()
    for plat_size in data2.keys():
        if data2[plat_size] > 0:
            midsummer.add_node(
                plat_size, size=data2[plat_size], nbliens=nb_liens[plat_size]
            )

    # Customize layout
    layout = go.Layout(
        title="""Présence des revues "française" dans...""",
        paper_bgcolor="rgba(0,0,0,0)",  # transparent background
        plot_bgcolor="rgba(0,0,0,0)",  # transparent 2nd background
        xaxis={"showgrid": False, "zeroline": False},  # no gridlines
        yaxis={"showgrid": False, "zeroline": False},  # no gridlines
    )

    # Get positions for the nodes in G
    pos_ = nx.spring_layout(midsummer)
    pos_["HAL"] = [0, 0]
    pos_["ROAD"] = [-2, 2]
    pos_["EZB"] = [-3, 0]
    pos_["MIAR"] = [-2, -2]
    pos_["Sherpa Romeo"] = [-1, -3]
    pos_["Scopus"] = [0, -4]
    pos_["ERIH PLUS"] = [1, -2]
    pos_["Wikipedia (français)"] = [2, 0]
    pos_["Twitter"] = [3, 1]
    pos_["Facebook"] = [4, 2]
    pos_["Latindex"] = [3, 3]
    pos_["OpenAlex"] = [-1, 2]
    pos_["X"] = [0, 3]
    pos_["WOS"] = [1, 2]
    # Make a node trace
    node_trace = go.Scatter(
        x=[],
        y=[],
        text=[],
        textposition="top center",
        textfont_size=10,
        mode="markers+text",
        hoverinfo="none",
        marker=dict(color=[], size=[], line=None),
    )

    node_trace2 = go.Scatter(
        x=[],
        y=[],
        text=[],
        textposition="middle center",
        textfont=dict(size=13, color="black"),
        mode="text",
        hoverinfo="none",
    )

    # For each node in midsummer, get the position and size and add to the node_trace
    for node in midsummer.nodes():
        x, y = pos_[node]
        node_trace["x"] += tuple([x])
        node_trace["y"] += tuple([y])
        node_trace["marker"]["color"] += tuple(["cornflowerblue"])
        node_trace["marker"]["size"] += tuple([3 * midsummer.nodes()[node]["size"]])
        node_trace["text"] += tuple(["<b>" + node + "</b>"])

        node_trace2["x"] += tuple([x])
        node_trace2["y"] += tuple([y])
        node_trace2["text"] += tuple(
            ["<b>" + str(midsummer.nodes()[node]["nbliens"]) + "</b>"]
        )

    # Create figure
    fig = go.Figure(layout=layout, )
    fig.add_trace(node_trace)  # Add node trace
    fig.add_trace(node_trace2)
    fig.update_layout(showlegend=False)  # Remove legend
    fig.update_xaxes(showticklabels=False)  # Remove tick labels
    fig.update_yaxes(showticklabels=False)  # Remove tick labels

    write_graph(fig,"8_autres_liens", OUTPUT_FORMAT)


# %% 9. Focus OpenEdition

def graphique_focus_OpenEdition(f_doaj_plateformes):
    pio.renderers.default = "browser"

    liste_titreid_OE = list(
        f_doaj_plateformes[f_doaj_plateformes["nom"] == "OpenEdition Journals"][
            "titreId"
        ]
    )
    f_doaj_plateformes = f_doaj_plateformes[
        f_doaj_plateformes["titreId"].isin(liste_titreid_OE)
    ]
    # On garde uniquement les titres et leurs accès de ceux qui ont un accès OE

    duplicates = f_doaj_plateformes[["titre", "nom"]]
    duplicates = duplicates.groupby(duplicates.columns.tolist(), as_index=False).size()
    data = duplicates.iloc[:, :-1]
    del duplicates

    data2 = data["nom"].value_counts()
    data2.name = "nb_titres"
    data2 = dict(data2)

    data2 = {k: v for k, v in data2.items() if v > 1}
    nb_revues = {k: v for k, v in data2.items()}
    data2 = {k: v / 3 for k, v in data2.items()}
    # data2 = {k: math.log(v)**1.8 for k, v in data2.items()}

    data3 = {}
    liste_titres = list(set(data["titre"]))

    for t in liste_titres:
        df = data[data["titre"] == t].nom.values.tolist()
        data3[t] = df

    data4 = {}
    for f in list(data2.keys()):
        tmp_dict = {}
        for j in list(data2.keys()):
            if j == f:
                next
            else:
                nb = 0
                for test in list(data3.values()):
                    if f in test and j in test:
                        nb += 1
                tmp_dict[j] = nb if nb == 0 else math.log(nb) ** 1.6
        data4[f] = tmp_dict

    # Add node for each plateforme
    midsummer = nx.Graph()
    for plat_size in data2.keys():
        if data2[plat_size] > 0:
            midsummer.add_node(
                plat_size, size=data2[plat_size], nbrevues=nb_revues[plat_size]
            )

    # For each co-titre between two plateformes, add an edge
    for plat in data4.keys():
        for co_plat in data4[plat].keys():
            # Only add edge if the count is positive
            if data4[plat][co_plat] > 0:
                midsummer.add_edge(plat, co_plat, weight=data4[plat][co_plat])

    # Customize layout
    layout = go.Layout(
        title="""Autres plateformes de diffusions""",
        paper_bgcolor="rgba(0,0,0,0)",  # transparent background
        plot_bgcolor="rgba(0,0,0,0)",  # transparent 2nd background
        xaxis={"showgrid": False, "zeroline": False},  # no gridlines
        yaxis={"showgrid": False, "zeroline": False},  # no gridlines
        width=1200, height=600
    )

    # Get positions for the nodes in G
    pos_ = nx.spring_layout(midsummer)
    pos_["OpenEdition Journals"] = [0, 0]
    pos_["Persée"] = [-1, 1]
    pos_["JSTOR"] = [-1.5, 0]
    pos_["Cairn.info"] = [1, 2]
    pos_["Gallica"] = [-1, -3]
    pos_["data.istex.fr"] = [-0.5, -4]
    pos_["Dialnet"] = [1, -2]

    # Custom function to create an edge between node x and node y, with a given text and width
    def make_edge(x, y, text, width):
        return go.Scatter(
            x=x,
            y=y,
            line=dict(width=width, color="cornflowerblue"),
            hoverinfo="text",
            text=([text]),
            mode="lines",
        )

    # For each edge, make an edge_trace, append to list
    edge_trace = []
    for edge in midsummer.edges():
        if midsummer.edges()[edge]["weight"] > 0:
            char_1 = edge[0]
            char_2 = edge[1]
            x0, y0 = pos_[char_1]
            x1, y1 = pos_[char_2]
            text = (
                    char_1 + "--" + char_2 + ": " + str(midsummer.edges()[edge]["weight"])
            )
            trace = make_edge(
                [x0, x1, None],
                [y0, y1, None],
                text,
                width=0.5 * midsummer.edges()[edge]["weight"] ** 1.75,
            )
            edge_trace.append(trace)

    # Make a node trace
    node_trace = go.Scatter(
        x=[],
        y=[],
        text=[],
        textposition="top center",
        textfont_size=10,
        mode="markers+text",
        hoverinfo="none",
        marker=dict(color=[], size=[], line=None),
    )

    # For each node in midsummer, get the position and size and add to the node_trace
    for node in midsummer.nodes():
        x, y = pos_[node]
        node_trace["x"] += tuple([x])
        node_trace["y"] += tuple([y])
        node_trace["marker"]["color"] += tuple(["cornflowerblue"])
        node_trace["marker"]["size"] += tuple([5 * midsummer.nodes()[node]["size"]])
        node_trace["text"] += tuple(
            [
                "<b>"
                + node
                + " ("
                + str(midsummer.nodes()[node]["nbrevues"])
                + " revues)"
                + "</b>"
            ]
        )

    # Create figure
    fig = go.Figure(layout=layout)
    for trace in edge_trace:
        fig.add_trace(trace)  # Add all edge traces
    fig.add_trace(node_trace)  # Add node trace
    fig.update_layout(showlegend=False)  # Remove legend
    fig.update_xaxes(showticklabels=False)  # Remove tick labels
    fig.update_yaxes(showticklabels=False)  # Remove tick labels
    write_graph(fig,"9_plateformes_focus_OE", OUTPUT_FORMAT)


if __name__ == "__main__":
    if len(sys.argv) == 2:  # (rétrocompatibilité)
        OUTPUT_FOLDER = sys.argv[1]
    elif len(sys.argv) > 2:
        for arg in sys.argv[1:]:
            if arg.startswith("--output_folder="):
                OUTPUT_FOLDER = arg.split("=", 1)[1]
            elif arg.startswith("--added_on_doaj_before="):
                ADDED_ON_DOAJ_BEFORE = arg.split("=", 1)[1]
            elif arg.startswith("--output_format="):
                OUTPUT_FORMAT = arg.split("=", 1)[1]
            else:
                print(f"Argument inconnu : {arg}")

    print(f"Output folder: {OUTPUT_FOLDER}")
    print(f"Added on DOAJ before: {ADDED_ON_DOAJ_BEFORE}")
    print(f"Output format: {OUTPUT_FORMAT}")

    print("Fetching data...")

    # Jeux de données
    doaj = pd.read_csv("{}/donnee/journalcsv.csv".format(str(FOLDER)), sep=",")
    doaj_revues_en_plus = pd.read_csv(
        "{}/donnee/revues_en_plus.csv".format(str(FOLDER)), sep=","
    )
    doaj_plateformes = pd.read_csv(
        "{}/donnee/doaj_plateformes.csv".format(str(FOLDER)), sep=","
    )
    doaj_editeurs = pd.read_csv("{}/donnee/doaj_editeurs.csv".format(str(FOLDER)), sep=",")
    doaj_historique = pd.read_csv(
        "{}/donnee/doaj_historique.csv".format(str(FOLDER)), sep=","
    )
    doaj_france_m = pd.read_csv("{}/donnee/doaj_france_m.csv".format(str(FOLDER)), sep=",")
    doaj_france = doaj[doaj["Country of publisher"] == "France"]
    doaj_france_complet = pd.concat([doaj_france, doaj_revues_en_plus])
    # Ajout des 18 revues françaises mais pas éditeur FR dans le DOAJ
    del doaj, doaj_revues_en_plus
    if ADDED_ON_DOAJ_BEFORE:
        print("Filtering Data...")
        doaj_france=doaj_france[doaj_france["Added on Date"] < ADDED_ON_DOAJ_BEFORE]
        doaj_france_complet=doaj_france_complet[doaj_france_complet["Added on Date"] < ADDED_ON_DOAJ_BEFORE]
        wanted_issn_e = set(doaj_france_complet['Journal EISSN (online version)'].dropna())
        wanted_issn_p = set(doaj_france_complet['Journal ISSN (print version)'].dropna())
        doaj_editeurs = doaj_editeurs[doaj_editeurs["Electronique"].isin(wanted_issn_e) | doaj_editeurs["Papier"].isin(wanted_issn_p)]
        doaj_plateformes = doaj_plateformes[doaj_plateformes["Electronique"].isin(wanted_issn_e) | doaj_plateformes["Papier"].isin(wanted_issn_p)]
        doaj_france_m = doaj_france_m[doaj_france_m["Electronique"].isin(wanted_issn_e) | doaj_france_m["Papier"].isin(wanted_issn_p)]
        doaj_plateformes.to_csv(
            "{}/filtered_doaj_plateforme.csv".format(str(OUTPUT_FOLDER)),
            encoding="utf-8",
        )
        doaj_editeurs.to_csv(
            "{}/filtered_doaj_editeurs.csv".format(str(OUTPUT_FOLDER)),
            encoding="utf-8",
        )

    print("Creating doaj visualisations...")

    revues_ajoutees_M22(doaj_france_complet)
    graphique_courbes(doaj_france_complet, doaj_france)
    graphique_variation_annuelle(doaj_france_complet)
    sceau_doaj(doaj_france_complet)
    graphiques_apc(doaj_france_complet)
    graphique_plateformes(doaj_plateformes)
    graphique_editeurs(doaj_editeurs)
    types_editeurs(doaj_editeurs)
    age_revues(doaj_historique, doaj_france_m, doaj_france_complet)
    graphique_langues(doaj_france_m)
    graphique_thematiques(doaj_france_m)
    graphique_autres_liens(doaj_france_m)
    graphique_focus_OpenEdition(doaj_plateformes)
    print("DOAJ visualisations created")
