import pandas as pd

folder = "../Graphiques_DOAJ"

corpus = pd.read_csv("{}/donnee/doaj_france_corpus_m.csv".format(str(folder)), sep=",")
doaj = pd.read_csv("{}/donnee/journalcsv.csv".format(str(folder)), sep=",")
mismatches = []

for i, row in corpus.iterrows():
    issn_e = row["issnelec"]
    issn_p = row["issnpapier"]

    if pd.notna(issn_e) and pd.notna(issn_p):
        # Trouver toutes les lignes dans le DOAJ qui contienne l'un de ces ISSN
        matches = doaj[
            (doaj['Journal EISSN (online version)'] == issn_e) |
            (doaj['Journal ISSN (print version)'] == issn_p)
            ]

        # Si ces ISSN sont trouvés dans plus d'une ligne du DOAJ
        if len(matches) == 1 :
            print(issn_p, issn_e,matches['Journal title'].iloc[0])
        elif len(matches) > 1:
            print(f"""Issns du titres : {row["id"]}""")
            print(f"Presents dans les lignes : {matches}")
