Mir@bel visualisations DOAJ
==========

Ces graphiques ont été réalisés dans le cadre du projet Mir@bel2022, lauréat de l'appel à projets du Fonds national pour la science ouverte FNSO 2.
Description et faits marquants du projet : https://reseau-mirabel.info/site/page/mirabel2022

Les données de ces graphiques proviennent d'une part d'un fichier csv téléchargeable sur le site du DOAJ (il contient la liste des revues du DOAJ avec leurs métadonnées)
et d'autre part de Mir@bel (via des requêtes en base de données et export csv).

Les graphiques ont été réalisés avec Python en utilisant les librairies matplotlib et plotly.

Installation
------------
```sh
# Create a local virtual environnment.
python3 -m venv venv
venv/bin/python3 -m pip install --upgrade pip
# Install dependencies.
venv/bin/pip install -r requirements.txt
mkdir -p Graphiques_DOAJ/donnee Graphiques_DOAJ/resultat
```

Utilisation
-----------
```sh
# Crée les fichiers CSV à partir de requêtes SQL sur Mir@bel
./bin/python src/create_csv.py [pathConfigYii]

# Génère les graphiques
./bin/python src/code_visualisations_doaj.py [outputDir]

# Avec plus de paramétrage 
./bin/python src/code_visualisations_doaj.py --output_folder [outputDir] --added_on_doaj_before=[2022-03-31] --output_format=[html|png]
```

Récupération des données
----------
Tous les fichiers doivent être placés dans le dossier Visualisations DOAJ\Données

 - journalcsv (téléchargé automatiquement) : https://doaj.org/csv
 
 - revues_en_plus (généré automatiquement) : revues considérées comme françaises dans Mir@bel (au moins un éditeur français) mais indiquées non françaises dans la colonne "Country of publisher" du fichier journalcsv.

	Exemple : https://reseau-mirabel.info/revue/13630/Animal un éditeur FR dans Mir@bel mais "Country of publisher" = Netherlands dans journalcsv.

	Pour obtenir une première liste de ces revues, via la recherche avancée Mir@bel : [Pays : France] [Avec lien DOAJ] [Sans attribut doaj-editeur-france]
	Attention, cette liste peut contenir du bruit. Ne pas garder les titres suivants :

		- Titres ayant bien "Country of publisher" = France dans journalcsv, mais n'ayant pas l'attribut attribut doaj-editeur-france dans Mir@bel.
		- Revues ayant leur ISSN papier sur un titre et leur ISSN électronique sur un autre titre dans Mir@bel.
		- Exemples : https://reseau-mirabel.info/revue/640, https://reseau-mirabel.info/revue/1344, https://reseau-mirabel.info/revue/1698, revue Anglophonia.

	Pour plus de détails, voir les notes de la grappe Mir@bel associée aux visualisations DOAJ.
	Une fois les titres non concernés enlevés, récupérer les lignes correspondantes à la liste dans le fichier journalcsv et les placer dans le fichier revues_en_plus.
	
 - doaj_france_m : première requête SQL du fichier SQL_visualisations_DOAJ.sql "Liste des titres français dans le DOAJ avec ISSNs"
 
 - doaj_plateformes : deuxième requête SQL du fichier SQL_visualisations_DOAJ.sql "Liste des accès des titres français dans le DOAJ (sans éditeur précédent)"
 
 - doaj_editeurs : troisième requête SQL du fichier SQL_visualisations_DOAJ.sql "Liste des éditeurs des titres français dans le DOAJ"
 
 - doaj_historique : quatrième requête SQL du fichier SQL_visualisations_DOAJ.sql "Historiques des titres français dans le DOAJ"

Licence
----------
GPLv3

Auteur initial
----------
Jérémie Léonard
